package com.example.jimen.realmapp.RecyclerAdapters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jimen.realmapp.Persona;
import com.example.jimen.realmapp.R;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class ListRecyclerViewAdapter extends RealmRecyclerViewAdapter<Persona, ListRecyclerViewAdapter.PersonaViewHolder> {


    public ListRecyclerViewAdapter(@Nullable OrderedRealmCollection<Persona> data, boolean autoUpdate) {
        super(data, autoUpdate);
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public PersonaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.persona_item, viewGroup, false);
        return new PersonaViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonaViewHolder holder, int i) {
        final Persona pers = getItem(i);
        holder.persona = pers;

        holder.dni.setText(pers.getDni());
        holder.nom.setText(pers.getNom());
        holder.edat.setText(String.valueOf(pers.getEdat()));
        holder.genere.setText(pers.getGenere());
    }

    class PersonaViewHolder extends RecyclerView.ViewHolder{

        public Persona persona;

        TextView dni;
        TextView nom;
        TextView edat;
        TextView genere;

        PersonaViewHolder(View view){
            super(view);

            dni = view.findViewById(R.id.pers_item_dni);
            nom = view.findViewById(R.id.pers_item_nom);
            edat = view.findViewById(R.id.pers_item_edat);
            genere = view.findViewById(R.id.pers_item_genere);

        }

    }
}
