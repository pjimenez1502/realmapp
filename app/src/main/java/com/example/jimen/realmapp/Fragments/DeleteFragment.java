package com.example.jimen.realmapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jimen.realmapp.Persona;
import com.example.jimen.realmapp.R;
import com.example.jimen.realmapp.RecyclerAdapters.DeleteRecyclerViewAdapter;

import io.realm.Realm;
import io.realm.RealmResults;

public class DeleteFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_personas, container, false);

        Realm realm = Realm.getDefaultInstance();

        RealmResults<Persona> results;

        results = realm.where(Persona.class).findAll();

        DeleteRecyclerViewAdapter recyclerViewAdapter = new DeleteRecyclerViewAdapter(results, true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);

        return view;


    }

}
