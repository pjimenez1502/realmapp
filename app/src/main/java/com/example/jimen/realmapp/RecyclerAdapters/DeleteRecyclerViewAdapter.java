package com.example.jimen.realmapp.RecyclerAdapters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jimen.realmapp.Persona;
import com.example.jimen.realmapp.R;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;

public class DeleteRecyclerViewAdapter extends RealmRecyclerViewAdapter<Persona, DeleteRecyclerViewAdapter.PersonaViewHolder> {


    public DeleteRecyclerViewAdapter(@Nullable OrderedRealmCollection<Persona> data, boolean autoUpdate) {
        super(data, autoUpdate);
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public PersonaViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int i) {
        final View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.persona_item, viewGroup, false);



        return new PersonaViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonaViewHolder holder, int i) {
        final Persona pers = getItem(i);
        holder.persona = pers;
        holder.persId = pers.getId();

        holder.dni.setText(pers.getDni());
        holder.nom.setText(pers.getNom());
        holder.edat.setText(String.valueOf(pers.getEdat()));
        holder.genere.setText(pers.getGenere());
    }

    class PersonaViewHolder extends RecyclerView.ViewHolder{

        public Persona persona;

        int persId;
        TextView dni;
        TextView nom;
        TextView edat;
        TextView genere;

        PersonaViewHolder(View view){
            super(view);

            dni = view.findViewById(R.id.pers_item_dni);
            nom = view.findViewById(R.id.pers_item_nom);
            edat = view.findViewById(R.id.pers_item_edat);
            genere = view.findViewById(R.id.pers_item_genere);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Realm realm = Realm.getDefaultInstance();

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            Persona persona = realm.where(Persona.class).equalTo("id", persId).findFirst();
                            persona.deleteFromRealm();

                        }
                    });
                }
            });

        }

    }
}
