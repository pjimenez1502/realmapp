package com.example.jimen.realmapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.jimen.realmapp.Persona;
import com.example.jimen.realmapp.R;

import io.realm.Realm;

public class EditPersonaFragment extends Fragment {

    Button sendPersbutton;
    EditText dniET;
    EditText nomET;
    EditText edatET;
    Spinner genSpinner;

    Persona pers;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_persona, container, false);

        dniET = view.findViewById(R.id.dniET);
        nomET = view.findViewById(R.id.nomET);
        edatET = view.findViewById(R.id.edatET);
        genSpinner = view.findViewById(R.id.genspinner);

        Bundle args = getArguments();
        int persId = args.getInt("ID");

        Realm realm = Realm.getDefaultInstance();

        Persona persona = realm.where(Persona.class).equalTo("id", persId).findFirst();
        this.pers = persona;

        if (persona != null) {
            dniET.setText(persona.getDni());
            nomET.setText(persona.getNom());
            edatET.setText(String.valueOf(persona.getEdat()));

            for (int i = 0; i < genSpinner.getCount(); i++) {
                if (genSpinner.getItemAtPosition(i).toString().equalsIgnoreCase(persona.getGenere())) {
                    genSpinner.setSelection(i);
                }
            }

        }

        ArrayAdapter<CharSequence> genSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.genSpinnerOptions, android.R.layout.simple_spinner_dropdown_item);
        genSpinner.setAdapter(genSpinnerAdapter);


        sendPersbutton = view.findViewById(R.id.sendPersona);

        sendPersbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (dniET.getText().toString().trim().equalsIgnoreCase("")) {
                    dniET.setError("No puede estar vacío");
                }else
                if (nomET.getText().toString().trim().equalsIgnoreCase("")) {
                    nomET.setError("No puede estar vacío");
                }else
                if (edatET.getText().toString().trim().equalsIgnoreCase("")) {
                    edatET.setError("No puede estar vacío");
                }else {
                    Realm realm = Realm.getDefaultInstance();

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            Persona persona = new Persona(pers.getId(), dniET.getText().toString(), nomET.getText().toString(), Integer.parseInt(edatET.getText().toString()), genSpinner.getSelectedItem().toString());
                            realm.copyToRealmOrUpdate(persona);

                        }
                    });

                    getFragmentManager().popBackStack();
                }
            }
        });

        return view;
    }



}
