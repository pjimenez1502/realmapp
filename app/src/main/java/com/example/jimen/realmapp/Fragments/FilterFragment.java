package com.example.jimen.realmapp.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.jimen.realmapp.R;

public class FilterFragment extends Fragment {

    Spinner genSpinner;

    Button entrebutton;
    Button majorbutton;
    Button menorbutton;
    Button generebutton;

    EditText entre1ET;
    EditText entre2ET;
    EditText majorET;
    EditText menorET;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter, container, false);


        genSpinner = view.findViewById(R.id.filt_genspinner);
        ArrayAdapter<CharSequence> genSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.genSpinnerOptions, android.R.layout.simple_spinner_dropdown_item);
        genSpinner.setAdapter(genSpinnerAdapter);

        entrebutton = view.findViewById(R.id.entrebutton);
        majorbutton = view.findViewById(R.id.majorbutton);
        menorbutton = view.findViewById(R.id.menorbutton);
        generebutton = view.findViewById(R.id.generebutton);

        entre1ET = view.findViewById(R.id.entre_1);
        entre2ET = view.findViewById(R.id.entre_2);
        majorET = view.findViewById(R.id.major);
        menorET = view.findViewById(R.id.menor);


        entrebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(entre1ET.getText().toString().equals("") || entre2ET.getText().toString().equals(""))) {
                    ListFragment listFragment = new ListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("Val", "0");
                    bundle.putInt("value1", Integer.parseInt(entre1ET.getText().toString()));
                    bundle.putInt("value2", Integer.parseInt(entre2ET.getText().toString()));
                    listFragment.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.fragment_container, listFragment).commit();
                }
            }
        });

        majorbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!majorET.getText().toString().equals("")) {
                    ListFragment listFragment = new ListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("Val", "1");
                    bundle.putInt("value1", Integer.parseInt(majorET.getText().toString()));
                    listFragment.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.fragment_container, listFragment).commit();
                }
            }
        });

        menorbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!menorET.getText().toString().equals("")) {
                    ListFragment listFragment = new ListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("Val", "2");
                    bundle.putInt("value1", Integer.parseInt(menorET.getText().toString()));
                    listFragment.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.fragment_container, listFragment).commit();
                }
            }
        });

        generebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListFragment listFragment = new ListFragment();
                Bundle bundle = new Bundle();
                bundle.putString("Val", "3");
                bundle.putString("value1", genSpinner.getSelectedItem().toString());
                System.out.println("###################");
                System.out.println(genSpinner.getSelectedItem().toString());
                System.out.println("###################");
                listFragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, listFragment).commit();
            }
        });



        return view;
    }
}
