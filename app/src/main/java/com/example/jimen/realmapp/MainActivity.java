package com.example.jimen.realmapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.jimen.realmapp.Fragments.AddPersonaFragment;
import com.example.jimen.realmapp.Fragments.DeleteFragment;
import com.example.jimen.realmapp.Fragments.EditFragment;
import com.example.jimen.realmapp.Fragments.ListFragment;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Realm.init(this);

//        RealmConfiguration config = new RealmConfiguration
//                .Builder()
//                .deleteRealmIfMigrationNeeded()
//                .build();
//        Realm.setDefaultConfiguration(config);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AddPersonaFragment(), null).commit();
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.create:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AddPersonaFragment(), null).commit();
                break;
            case R.id.list:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListFragment(), null).commit();
                break;
            case R.id.edit:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new EditFragment(), null).commit();
                break;
            case R.id.delete:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DeleteFragment(), null).commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
