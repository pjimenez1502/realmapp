package com.example.jimen.realmapp;

import android.util.Log;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Persona extends RealmObject {

    @PrimaryKey
    int id;
    @Required
    String dni;
    @Required
    String nom;
    int edat;
    String genere;

    public Persona(){
    }

    public Persona(int id, String dni, String nom, int edat, String genere){
        this.id = id;
        this.dni = dni;
        this.nom = nom;
        this.edat = edat;
        this.genere = genere;
    }

    public String getDni() {
        return dni;
    }

    public String getNom() {
        return nom;
    }

    public int getEdat() {
        return edat;
    }

    public int getId() {
        return id;
    }

    public String getGenere() {
        return genere;
    }

}
