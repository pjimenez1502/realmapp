package com.example.jimen.realmapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.jimen.realmapp.Persona;
import com.example.jimen.realmapp.R;

import io.realm.Realm;

public class AddPersonaFragment extends Fragment {

    Button sendPersbutton;
    EditText dniET;
    EditText nomET;
    EditText edatET;
    Spinner genSpinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_persona, container, false);

        dniET = view.findViewById(R.id.dniET);
        nomET = view.findViewById(R.id.nomET);
        edatET = view.findViewById(R.id.edatET);
        genSpinner = view.findViewById(R.id.genspinner);


        ArrayAdapter<CharSequence> genSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.genSpinnerOptions, android.R.layout.simple_spinner_dropdown_item);
        genSpinner.setAdapter(genSpinnerAdapter);


        sendPersbutton = view.findViewById(R.id.sendPersona);

        sendPersbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (dniET.getText().toString().trim().equalsIgnoreCase("")) {
                    dniET.setError("No puede estar vacío");
                }else
                if (nomET.getText().toString().trim().equalsIgnoreCase("")) {
                    nomET.setError("No puede estar vacío");
                }else
                if (edatET.getText().toString().trim().equalsIgnoreCase("")) {
                    edatET.setError("No puede estar vacío");
                }else {
                    Realm realm = Realm.getDefaultInstance();

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            Number currentIdNum = realm.where(Persona.class).max("id");
                            int nextId;
                            if(currentIdNum == null) {
                                nextId = 1;
                            } else {
                                nextId = currentIdNum.intValue() + 1;
                            }

                            Persona persona = new Persona(nextId, dniET.getText().toString(), nomET.getText().toString(), Integer.parseInt(edatET.getText().toString()), genSpinner.getSelectedItem().toString());
                            realm.insert(persona);

                        }
                    });

                    dniET.setText("");
                    nomET.setText("");
                    edatET.setText("");
                }
            }
        });

        return view;
    }



}
