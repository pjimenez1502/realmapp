package com.example.jimen.realmapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.jimen.realmapp.Persona;
import com.example.jimen.realmapp.R;
import com.example.jimen.realmapp.RecyclerAdapters.ListRecyclerViewAdapter;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class ListFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    Button filterbutton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_list_personas, container, false);

        filterbutton = view.findViewById(R.id.filter_button);

        filterbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity)view.getContext()).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FilterFragment()).addToBackStack(null).commit();
            }
        });




        Realm realm = Realm.getDefaultInstance();

        RealmQuery<Persona> persQuery;
        RealmResults<Persona> results;


        results = realm.where(Persona.class).findAll();

        if (!(getArguments() == null)) {
            if (!(getArguments().get("Val") == null)) {

                String value = getArguments().get("Val").toString();

                switch (value) {
                    case "0":
                        results = realm.where(Persona.class)
                                .between("edat", getArguments().getInt("value1"), getArguments().getInt("value2"))
                                .findAll();
                        break;
                    case "1":
                        results = realm.where(Persona.class)
                                .greaterThan("edat", getArguments().getInt("value1"))
                                .findAll();
                        break;
                    case "2":
                        results = realm.where(Persona.class)
                                .lessThan("edat", getArguments().getInt("value1"))
                                .findAll();
                        break;
                    case "3":
                        results = realm.where(Persona.class)
                                .equalTo("genere", getArguments().getString("value1"))
                                .findAll();
                        break;
                }
            }
        }


        ListRecyclerViewAdapter recyclerViewAdapter = new ListRecyclerViewAdapter(results, true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);

        return view;
    }

}
